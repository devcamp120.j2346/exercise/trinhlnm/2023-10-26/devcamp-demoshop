import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css';

class Footer extends Component {
    render() {
        return <>
            <div className="shop-footer">
                <div className="container">
                    <div className="row" style={{paddingTop: "20px"}}>
                        <div className="col-md-4 col-xl-5">
                            <div className="pr-xl-4">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p>©  2018 . All Rights Reserved.</p>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <h5>Contacts</h5>
                            <dl className="contact-list">
                                <dt>Address:</dt>
                                <dd>Kolkata, West Bengal, India</dd>
                            </dl>
                            <dl className="contact-list">
                                <dt>email:</dt>
                                <dd>
                                    <a href="">info@example.com</a>
                                </dd>
                            </dl>
                            <dl className="contact-list">
                                <dt>phones:</dt>
                                <dd>
                                    <a href="">+91 99999999</a>
                                    <span> or </span>
                                    <a href="">+91 11111111</a>
                                </dd>
                            </dl>
                        </div>

                        <div className="col-md-4 col-xl-3">
                            <h5>Links</h5>
                            <ul>
                                <li style={{marginBottom: ".5rem"}}><a href="">About</a></li>
                                <li style={{marginBottom: ".5rem"}}><a href="">Projects</a></li>
                                <li style={{marginBottom: ".5rem"}}><a href="">Blog</a></li>
                                <li style={{marginBottom: ".5rem"}}><a href="">Contacts</a></li>
                                <li style={{marginBottom: ".5rem"}}><a href="">Pricing</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="row no-gutters social-container">
                    <div className="col">
                        <a className="social-inner" href="">
                            <span>Facebook</span>
                        </a>
                    </div>
                    <div className="col">
                        <a className="social-inner" href="">
                            <span>instagram</span>
                        </a>
                    </div>
                    <div className="col">
                        <a className="social-inner" href="">
                            <span>twitter</span>
                        </a>
                    </div>
                    <div className="col">
                        <a className="social-inner" href="">
                            <span>google</span>
                        </a>
                    </div>
                </div>
            </div>
        </>;
    }
}

export default Footer;