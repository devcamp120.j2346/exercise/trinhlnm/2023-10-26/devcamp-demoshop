import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css';
import productsJ from './ProductData.json';

class ContentComponent extends Component {
    render() {
        const products9 = productsJ.Products.slice(0, 9);
        console.log(products9);

        return <>
            <div className="container main-container">
                <div className="container">
                    <h3 className="center">Product List</h3>
                    <p>Showing 1 - 9 of 24 products</p>
                    <div className="row">
                        {
                            products9.map((e) => (
                                <div className="col-6 col-sm-4">
                                    <div className="product-box card bg-light mb-3">
                                        <div className="card-header">
                                            <h5 className="card-title text-center">
                                                <a className="a-product" href="">{e.Title}</a>
                                            </h5>
                                        </div>
                                        <div className="card-body">
                                            <div className="text-center" style={{height: "12rem"}}>
                                                <a href="">
                                                    <img
                                                        src={e.ImageUrl}
                                                        className="card-img-top"
                                                        style={{height: "12rem"}}
                                                    />
                                                </a>
                                            </div>
                                            <p className="card-text description">{e.Description}</p>
                                            <p><b>Category:</b> {e.Category}</p>
                                            <p><b>Made by: </b> {e.Manufacturer}</p>
                                            <p><b>Organic:</b> {e.Organic == true ? "Yes" : "No"}</p>
                                            <p><b>Price:</b> ${e.Price}</p>
                                            <div className="text-center">
                                                <button className="btn btn-primary" style={{ marginBottom: "0.5rem" }}>Add to Cart</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </>;
    }
}

export default ContentComponent;