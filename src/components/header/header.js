import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css'
import IconComponent from "./icon";

class Header extends Component {
    render() {
        return <>
            <div className="container text-center">
                <div className="logo">
                    <IconComponent/>
                </div>
                <h1 style={{fontSize: "2.5rem"}}>React Store</h1>
                <p>Demo App Shop24h v1.0</p>
            </div>

            <nav className="navbar navbar-expand-md bg-dark navbar-dark extra-padd">
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link active">Home</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </>;
    }
}

export default Header;